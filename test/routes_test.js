const chai = require("chai");
const { assert } = require("chai");
const http = require("chai-http");
chai.use(http);

describe("Test currency rates", () => {
  it("test_api_get_currency_is_running", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/forex/currency")
      .end((err, res) => {
        assert.isDefined(res);
        done();
      });
  });
  it("test_api_post_returns_400_if_name_is_missing", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        name: "brBoyd87",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("test_api_post_returns_400_if_name_is_not_a_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        name: 12,
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("test_api_post_returns_400_if_name_is_empty_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        name: "",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("test_api_post_returns_400_if_ex_is_missing", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        ex: "brBoyd87",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("test_api_post_returns_400_if_ex_is_not_an_object", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        ex: "brBoyd87",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("test_api_post_returns_400_if_ex_is_empty_object", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        ex: "",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("test_api_post_returns_400_if_alias_is_missing", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        alias: "brBoyd87",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("test_api_post_returns_400_if_alias_is_not_an_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        alias: 15,
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("test_api_post_returns_400_if_alias_is_empty_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        alias: "",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });

  it("test_api_post_returns_400_if_all_fields_are_complete_but_alias_is_duplicate", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        alias: "yen",
        name: "Jdsadas",
      })
      .end((err, res) => {
        assert.equal(res.status, 400);
        done();
      });
  });
  it("test_api_post_returns_400_if_all_fields_are_complete_but_no_duplicates", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/forex/currency")
      .type("json")
      .send({
        alias: "won",
        name: "South Korean Won",
        ex: {
          peso: 0.043,
          usd: 0.00084,
          yen: 0.092,
          yuan: 0.0059,
        },
      })
      .end((err, res) => {
        assert.notEqual(res.status);
        done();
      });
  });
});
